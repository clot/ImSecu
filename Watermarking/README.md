# Watermarking

Report done by William CLOT and Camille PLAYS.

Last update: 26/04/2018.

* [**Exercice 1 : LSB**](#exercice-1-lsb)
	- InsertNoiseLSB()
	- ExtractNoiseLSB()
	- Testing
* [**Exercice 2 : LSB + CRC**](#exercice-2-lsb-crc)
	- InsertCRCLSB()
	- ExtractCRCLSB()
	- Testing
* [**Exercice 3 : LSB + CRC + Self-Embedding**](#exercice-3-lsb-crc-self-tampering)
	- BlockMean()
	- InsertSelfEmbeddingLSB()
	- ExtractSelfEmbeddingLSB()
	- Testing
* [Conclusion](#conclusion)

## Exercice 1 : LSB

The objective in this exercice is to extract all the LSB (Least Significant Bits) of the image and replace it with a pseudo random noise. We will then be able to detect if the image has been tampered or not by extracting this pseudo random noise and comparing it to the expected noise we placed there in the first place.

They are two functions in this exercice, `InsertNoiseLSB()` and `ExtractNoiseLSB()`.

#### InsertNoiseLSB()

`InsertNoiseLSB()` will loop through the RGB components and loop through every pixel in the image and place a random bit called `noise` in the last significant bit of the array.

```c++
int CImage::InsertNoiseLSB(){
	int noise;
	srand(0); // The seed of the random generator

	// c is the component RGB
	// pixel is the current pixel, an 8 bit value of the component

	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;
		for (int pixel=0; pixel<width*height;pixel++) {
			noise = rand() % 2; // random bit
			SETBIT(C[c][pixel], 0, noise); //placing the bit
		}
	}
	return SUCCESS;
}
```

#### ExtractNoiseLSB

`ExtractNoiseLSB()` will loop through the RGB component and loop through every pixel and extract the LSB and check if it is the same as the expected random bit. To do so we just use the same seed for the random generator and go through the pixels in the same order.
If the random bit matches we put a `00000000` in the pixel value and if ever there is a mismatch we put a `11111111` in the pixel value.


```c++
int CImage::ExtractNoiseLSB(){
	int noise;
	srand(0); // We keep the same seed generator
	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;
		for (int pixel=0; pixel<width*height;pixel++) {
			noise = rand() % 2; //Expected random bit
			if (GETBIT(C[c][pixel],0) == noise){
                //if it matches we but 0 on all the bits of the pixel
				for (int i=0; i<8; i++){
					SETBIT(C[c][pixel], i, 0);
				}
			}
			else{
                //if it doesn't match we but 1 on all the bits of the pixel
				for (int i=0; i<8; i++){
					SETBIT(C[c][pixel], i, 1);
				}
			}
		}
	}      
	return SUCCESS;
}
```

#### Testing

I tested my program with this image of a bird:

 Original | LSB | LSB Tampered | Checked
 :-: | :-: | :-: | :-: | :-:
 ![original](VC2010/images/exercice1/bird.jpg)| ![original](VC2010/images/exercice1/bird.jpg) | ![original](VC2010/images/exercice1/LSB_tampered.jpg) | ![original](VC2010/images/exercice1/checked.jpg)

 I first inserted the pseudo random noise with the command:

 ```bash
 tp.exe -i1 bird.ppm
```

and then modified the resulting image by adding a colored box to the image. I then verified if my image was tampered or not by running the following command:

```bash
tp.exe -x1 LSB_tampered.ppm
```

We then got a black image with a box of colored pixels where the pixels were tampered.

What's good about this method is that there is a pixel by pixel tampering detection. It's a relatively simple process and easy to implement.

On the downside it is relatively easy to by-pass, if ever somebody changes completly the image without modifying the LSB this method will not detect any changes. Furthermore as our generated noise all starts with a simple seed, somebody could find the seed a create it's own watermarking.

## Exercice 2 : LSB + CRC

The objective in this exercice is to find a way to make authentication data dependent of the considered image. For that, the image will be split in blocks and a check sum representing each block will be computed from the seven MSB and stored in the LSB. The authentication is assured by the computation of the CRC which depends on a unique crcTable which has to be known by the author.

#### InsertCRCLSB()

The InsertCRCLSB() function computes the checksum value and embed it in the LSB. For that, it uses the function CRCBlock which outputs a CRC value encoded in 32 bits. Then this value is stored in the LSB of the first 32 pixels of the corresponding block.


```c++
int CImage::InsertCRCLSB(){
	int nb_xblocks = width/8;
	int nb_yblocks = height/8;

	unsigned long crc;
	unsigned long *crcTable;
	short bit;

	CRCTable(&crcTable);

	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;

		for (int i=0; i<nb_xblocks; i++)
			for (int j=0; j<nb_yblocks; j++) {
				//Checksum
				crc = CRCBlock(c, i*8, j*8, 8, 8, crcTable);
				for (int k=0; k<4; k++) {
					for (int l=0; l<8; l++) {
						//placing checksum in 32 first bits of each block
						bit = GETBIT(crc, k*8+l);
						SETBIT(C[c][(j*8*width+i*8)+(k*width+l)],0,bit);
					}
				}
			}
		}

	free(crcTable);

	return SUCCESS;
}
```

#### ExtractCRCLSB()

The ExtractCRCLSB() function will extract the 32 first LSB of each block and store it in xcrc. It will also compute the checksum itself in crc and then compares crc with xcrc and decides if the block has been tampered or not. If the block has been tampered, a pattern of 'bad blocks' is drawn instead of the tampered block.


```c++
int CImage::ExtractCRCLSB(){
	int nb_xblocks = width/8;
	int nb_yblocks = height/8;
	unsigned long crc, xcrc;
	unsigned long *crcTable;
	short bit;

	CRCTable(&crcTable);

	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;

		for (int i=0; i<nb_xblocks; i++)
			for (int j=0; j<nb_yblocks; j++) {
				//computes the checksum of each block
				crc = CRCBlock(c, i*8, j*8, 8, 8, crcTable);
				for (int k=0; k<4; k++) {
					for (int l=0; l<8; l++) {
						//getting 32 first LSB of each block
						bit = GETBIT(C[c][(j*8*width+i*8)+(k*width+l)],0);
						SETBIT(xcrc, k*8+l,bit);
					}
				}
				//checking the authenticity of the block
				if (crc != xcrc){
					DrawBadBlock(i*8,j*8,8,8);
				}
			}
		}

	free(crcTable);

	return SUCCESS;
}
```


#### Testing


After watermarking the bird image with the command:
```bash
tp -i2 <bird.ppm>
```
Then, we tampered the watermarked image and we checked the integrity of the image with the command:


```bash
tp -x2 <_CRC_tampered.ppm>
```


Original | Watermarked | Watermarked Tampered | Checked
:-: | :-: | :-: | :-: | :-:
![original](VC2010/images/exercice2/bird.jpg)| ![original](VC2010/images/exercice2/_CRC.jpg) | ![original](VC2010/images/exercice2/_CRC_tampered.jpg) | ![original](VC2010/images/exercice2/_checked.jpg)



##### Comment the results
We obtain an image with a 'bad zone' full of 'bad blocks' which is exactly the area that we tampered. Indeed, by changing slightly the values of the pixels on this zone, we changed the values used in the checksum and we also possibly changed the result of the checksum (the LSB). Therefore, when the program verifies the integrity by recomputing the checksum, it obtains different results from the one stored in the LSB and knows that a block has been tampered.

##### Which are the improvements in terms of reliablity
In this second algorithm, we can check the integrity of an image on any bits and not just on the LSB. Indeed, if some MSB are changed, the checksum will very likely change as well. Moreover, an attacker can't try to recompute the new checksum of the tampered blocks since he doesn't know the crcTable, which is the 'key' of the image.


##### In the function CRCBlocks(), what is the reason for zeroing the pixels LSB?


## Exercice 3 : LSB + CRC + Self-Tampering

In this exercice we will use the last 32 bits of the LSB blocks that aren't used to keep a mean value of a sub-block to be able to restore aproximatively the content of tampered block.

#### BlockMean()

We first wrote a `BlockMean()` function that computes the average intensity level of a block in component c.

```c++
int CImage::BlockMean(short *value, int c, int posx, int posy,
							 int block_width, int block_height){
	int sum = 0;

	if (C[c]==NULL)
		return ERROR;
	if (posx<0 || posy<0 || posx+block_width-1>width
		 ||posy+block_height-1>height)
		return ERROR;
	for (int i=0; i<block_width; i++){
		for (int j=0; j<block_height; j++){
			// Computes the sum
			sum = sum + C[c][posy*width+posx+i+j*width];
		}
	}
	// Computes the average
	(*value) = (short)(sum/(block_width*block_height));

	return SUCCESS;
}
```
This function just goes through all the pixels in a block and adds the value of the level intensity in a sum variable and devides it by the total number of pixels in the block to obtain the average value of the block.

#### InsertSelfEmbeddingLSB()

We then completed the `InsertSelfEmbeddingLSB()` which adds the checksum and the restoration bits in the LSB of the image:
```c++
int CImage::InsertSelfEmbeddingLSB(){
	int nb_xblocks = width/8;
	int nb_yblocks = height/8;
	unsigned long crc;
	unsigned long *crcTable;
	short bit;
	short m[4];
	int ti,tj,k,l;

	CRCTable(&crcTable);
	//Block translation values
	ti = 4;
	tj = 4;

	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;

		for (int i=0; i<nb_xblocks; i++)
			for (int j=0; j<nb_yblocks; j++) {
				//Compute checksum
				crc = CRCBlock(c, i*8, j*8, 8, 8, crcTable);

				//Placing checksum in 32 first bits of each block
				for (int u=0; u<8; u++) {
					for (int v=0; v<4; v++) {
						bit = GETBIT(crc, v*8+u);
						SETBIT(C[c][(j*8*width+i*8)+(v*width+u)],0,bit);
					}
				}

				//Initiating the values of the m
				for (int blocknb=0; blocknb<4; blocknb++){
					m[blocknb]=0;
				}

				//Calculating the 4 mean values of the 4 sub-blocks
				for (int block=0; block<4; block++){
					if (block==0){
						BlockMean(&m[block], c, i*8, j*8, 4, 4);
					}
					else if (block==1){
						BlockMean(&m[block], c, i*8+4, j*8, 4, 4);
					}
					else if (block==2){
						BlockMean(&m[block], c ,i*8, j*8+4, 4, 4);
					}
					else if (block==3){
						BlockMean(&m[block], c, i*8+4, j*8+4, 4, 4);
					}
				}

				//Calculating the position of the block B(k,l) that keeps the mean values of B(i,j)
				k = (i + ti) % nb_xblocks;
				l = (j + tj) % nb_yblocks;

				//Placing the mean values in the translated block B(k,l)
				//in the 32 last bits of the LSB's block
				for (int v=4; v<8; v++) {
					for (int u=0; u<8; u++) {
						int restoration_bit = GETBIT(m[v-4], u);
						SETBIT(C[c][(l*8*width+k*8)+v*width+u], 0, restoration_bit);
					}
				}
			}
	}

	free(crcTable);

	return SUCCESS;
}
```

We loop through every block of the image and compute the checksum of the block and place it in the first 32 bits of the LSB of the block. We then devide each block in four and compute the mean of each sub-block with the previous function and store these restoration values in the array m[0] to m[3].

We then compute the new position of the translated block with a modulous on the number of blocks on the x and y axis and position these restoration bits in the last 32 bits of the LSB of the translated block B(k,l).

#### ExtractSelfEmbeddingLSB()

The last function `ExtractSelfEmbeddingLSB()` is used to extract the LSB of the image and check the checksum to make sure that the image hasn't been tampered. If the image has been tampered we then extract the restoration bits from the translated B(k,l) block and assign this value to the sub-block's pixel value.


```c++
int CImage::ExtractSelfEmbeddingLSB(){
	int nb_xblocks = width/8;
	int nb_yblocks = height/8;
	unsigned long crc, xcrc=0;
	unsigned long *crcTable;
	short bit;
	short m[4];
	int ti,tj,k,l;

	CRCTable(&crcTable);
	//Block translation values
	ti = 4;
	tj = 4;

	for (int c=0; c<nb_comp;c++) {
		if (C[c]==NULL)
			return ERROR;

		for (int i=0; i<nb_xblocks; i++)
			for (int j=0; j<nb_yblocks; j++){
				//Checksum of the current image
				crc = CRCBlock(c, i*8, j*8, 8, 8, crcTable);
				//getting the checksum from LSB of the 32 first bits of the block and placing them in xcrc
				for (int v=0; v<4; v++) {
					for (int u=0; u<8; u++) {
						bit = GETBIT(C[c][(j*8*width+i*8)+(v*width+u)],0);
						SETBIT(xcrc, v*8+u, bit);
					}
				}
				//checking the authenticity of the block
				if (xcrc != crc){
					//Position of the new block B(k,l)
					k = (i + ti) % nb_xblocks;
					l = (j + tj) % nb_yblocks;

					//Initiating the m array
					for (int nbblock=0; nbblock<4; nbblock++){
						m[nbblock]=0;
					}

					//Extracting the restoration values hidden in the block B(k,l) and putting them into the m array
					for (int v=4; v<8; v++) {
						for (int u=0; u<8; u++) {
							int restoration_bit = GETBIT(C[c][l*8*width+k*8+v*width+u],0);
							SETBIT(m[v-4],u,restoration_bit);
						}
					}

					//Reconstruct the 4 tampered sub-blocks
					for (int block=0; block<4; block++){
						if (block==0){
							DrawFlatBlock(c,i*8,j*8,4,4,m[block]);
						}
						else if (block==1){
							DrawFlatBlock(c,i*8+4,j*8,4,4,m[block]);
						}
						else if (block==2){
							DrawFlatBlock(c,i*8,j*8+4,4,4,m[block]);
						}
						else if (block==3){
							DrawFlatBlock(c,i*8+4,j*8+4,4,4,m[block]);
						}
					}
				}
			}
	}
	free(crcTable);
	return SUCCESS;
}
```

First of all this function computes the checksum and stores it in the crc variable and extracts the checksum from the blocks and stores it in xcrc. It then checks if the image has been tampered by comparing crc and xcrc. If the image has been tampered it will then extract the restoration bits stored in the translated block B(k,l) and put them in the array m[0] to m[3].
We then put these restoration values back into each sub-block of the current B(i,j) block by using the function `DrawFlatBlock()`.

#### Testing

After watermarking the bird image with the command:
```bash
tp -i3 <bird.ppm>
```
Then, we tampered the watermarked image and we checked the integrity of the image with the command:


```bash
tp -x3 <_CRC_tampered.ppm>
```


Original | Watermarked | Watermarked Tampered | Reconstructed
:-: | :-: | :-: | :-: | :-:
![original](VC2010/images/exercice3/bird.jpg)| ![original](VC2010/images/exercice3/bird.jpg) | ![original](VC2010/images/exercice3/Self.jpg) | ![original](VC2010/images/exercice3/checked.jpg)

##### Comment the results

We managed to reconstruct approximately the tampered block thanks to the restoration bits. The restoration is not perfect since it gives the same value to 16 pixels (a sub-block) instead of one value per pixels. Moreover, as it is an average value, this value is not necessarily present in the image and, by reconstructing an area, we can add new values that didn't exist in the original image and give this fake aspect.

##### Why is it necessary to embed the restoration bits in a distant block?

We can think of a general case where someone would want to suppress an element in the image. In that case, the image will be tampered locally on a restricted area. Embedding the restoration bits in a distant block prevents the one who tampers the image to overwrite the restoration bits and therefore allows the authors to reconstruct the tampered area.

##### What is the limit of restoration functionalities

The bigger the tampered zone is, the more likely it is for the one who tampers to overwrite the restoration bits of the tampered zone. The same things apply if one tampers several zones on the image distant from each other, then the restoration zone could be also overwritten. Overwriting the restoration zone could be very bad because wrong values would be used and the restored area would be completely different from the original one.



## Conclusion

##### Comparison of the 3 methods in term of visibility, reliability, complexity and offered functionalities



Method | Visibility| Reliability | Complexity | Offered functionalities
:-: | :-: | :-: | :-: | :-: | :-:
LSB |  Only affects LSB (isn't perceptible) |  Random noise: can be generated by the same seed + we don't notice if any other bit different from the LSB is modified | Just need to generate random numbers | Spot a tampered area
LSB + CRC | Only affects LSB (isn't perceptible) | Any significant modification of bits is perceptible but if the attacker knows the crcTable, he can recompute the new checksums of the tampered area | Need to compute the crc value of each block | Spot a tampered area
LSB + CRC + self_embedding | Only affects LSB (isn't perceptible)  | Doesn't work if the tampered area is too big | Need to compute a crc value for each block and an average intensity level for each sub-block and store it in translated blocks | Reconstruct the original area from a tampered area



##### Common disadvantage of the three methods


In all of the methods, it is impossible for the author to reconstruct exactly the image because there is a loss of data. Indeed, in the LSB method, the LSB is replaced by a random noise and therefore the original value of the LSB is lost. The same thing goes with the LSB-CRC method which overwrites half of the LSB with the checksum. In the LSB+CRC+Self-embedding, half of the LSB are overwritten with a check-sum and the other half with restoration values.
